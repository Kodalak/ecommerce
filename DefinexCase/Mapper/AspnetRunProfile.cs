﻿using DefinexCase.ViewModels;
using AutoMapper;
using DefinexCase.Models;

namespace DefinexCase.Mapper
{
    public class AspnetRunProfile : Profile
    {
        public AspnetRunProfile()
        {
            CreateMap<ProductModel, ProductViewModel>().ReverseMap();
            CreateMap<CategoryModel, CategoryViewModel>().ReverseMap();
            CreateMap<CartModel, CartViewModel>().ReverseMap();
            CreateMap<CartItemModel, CartItemViewModel>().ReverseMap();
        }
    }
}
