﻿using DefinexCase.ViewModels.Base;
using System.Collections.Generic;

namespace DefinexCase.ViewModels
{
    public class CompareViewModel : BaseViewModel
    {
        public string UserName { get; set; }
        public List<ProductViewModel> Items { get; set; } = new List<ProductViewModel>();
    }
}
