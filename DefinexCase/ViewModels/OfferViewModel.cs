﻿using DefinexCase.Models;
using DefinexCase.ViewModels.Base;
using System;
using System.Collections.Generic;

namespace DefinexCase.ViewModels
{
    public class OfferViewModel : BaseViewModel
    {
        public string OfferName { get; set; }
        public decimal? GrandTotal { get; set; }

        public static implicit operator OfferViewModel(OfferModel v)
        {
            throw new NotImplementedException();
        }
    }
}
