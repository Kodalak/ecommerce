﻿using System.Linq;
using System.Threading.Tasks;
using DefinexCase.Repositories;
using DefinexCase.Specifications;
using DefinexCase.Data;
using DefinexCase.Repository.Base;

namespace DefinexCase.Repository
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
        public CartRepository(AspnetRunContext dbContext) : base(dbContext)
        {
        }

        public async Task<Cart> GetByUserNameAsync(string userName)
        {
            var spec = new CartWithItemsSpecification(userName);
            return (await GetAsync(spec)).FirstOrDefault();
        }
    }
}
