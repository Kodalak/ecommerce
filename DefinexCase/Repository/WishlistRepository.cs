﻿
using DefinexCase.Base;
using DefinexCase.Repositories;
using DefinexCase.Specifications;
using DefinexCase.Data;
using DefinexCase.Repository.Base;
using System.Linq;
using System.Threading.Tasks;

namespace DefinexCase.Repository
{
    public class WishlistRepository : Repository<Wishlist>, IWishlistRepository
    {
        public WishlistRepository(AspnetRunContext dbContext) : base(dbContext)
        {
        }

        public async Task<Wishlist> GetByUserNameAsync(string userName)
        {
            var spec = new WishlistWithItemsSpecification(userName);
            return (await GetAsync(spec)).FirstOrDefault();
        }
    }
}
