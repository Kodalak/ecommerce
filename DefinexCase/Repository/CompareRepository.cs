﻿
using DefinexCase.Base;
using DefinexCase.Repositories;
using DefinexCase.Specifications;
using DefinexCase.Data;
using DefinexCase.Repository.Base;
using System.Linq;
using System.Threading.Tasks;

namespace DefinexCase.Repository
{
    public class CompareRepository : Repository<Compare>, ICompareRepository
    {
        public CompareRepository(AspnetRunContext dbContext) : base(dbContext)
        {
        }

        public async Task<Compare> GetByUserNameAsync(string userName)
        {
            var spec = new CompareWithItemsSpecification(userName);
            return (await GetAsync(spec)).FirstOrDefault();
        }
    }
}
