﻿
using DefinexCase.Base;
using DefinexCase.Repositories;
using DefinexCase.Data;
using DefinexCase.Repository.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DefinexCase.Repository
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(AspnetRunContext dbContext) : base(dbContext)
        {
        }

        public Task<IEnumerable<Order>> GetOrderByUserNameAsync(string userName)
        {
            throw new NotImplementedException();
        }
    }
}
