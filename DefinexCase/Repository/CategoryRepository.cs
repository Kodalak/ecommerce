﻿
using DefinexCase.Base;
using DefinexCase.Repositories;
using DefinexCase.Specifications;
using DefinexCase.Data;
using DefinexCase.Repository.Base;
using System.Linq;
using System.Threading.Tasks;

namespace DefinexCase.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(AspnetRunContext dbContext) : base(dbContext)
        {            
        }

        //public async Task<Category> GetCategoryWithProductsAsync(int categoryId)
        //{            
        //    var spec = new CategoryWithProductsSpecification(categoryId);
        //    var category = (await GetAsync(spec)).FirstOrDefault();
        //    return category;
        //}
    }
}
