﻿using DefinexCase.ViewModels;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface ICheckOutPageService
    {
        Task<CartViewModel> GetCart(string userName);        
        Task CheckOut(OrderViewModel order, string userName);
    }
}
