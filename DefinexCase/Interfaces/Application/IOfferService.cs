﻿using DefinexCase.Models;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface IOfferService
    {
        OfferModel FindBestOffer(CartModel cart);
    }
}
