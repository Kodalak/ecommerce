﻿using DefinexCase.Models;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface ICartService
    {
        Task<CartModel> GetCartByUserName(string userName);
        Task AddItem(string userName, int productId);
        Task RemoveItem(int cartId, int cartItemId);
        Task ClearCart(string userName);
        Task<int> GetCartCountByUserName(string name);
    }
}
