﻿using DefinexCase.Models;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface IOrderService
    {        
        Task<OrderModel> CheckOut(OrderModel orderModel);
    }
}
