﻿using DefinexCase.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryModel>> GetCategoryList();
    }
}
