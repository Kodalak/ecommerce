﻿using DefinexCase.ViewModels;
using System.Threading.Tasks;

namespace DefinexCase.Interfaces
{
    public interface IComparePageService
    {
        Task<CompareViewModel> GetCompare(string userName);
        Task RemoveItem(int compareId, int productId);
        Task AddToCart(string userName, int productId);
    }
}
