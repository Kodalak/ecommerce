﻿using DefinexCase.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DefinexCase.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetOrderByUserNameAsync(string userName);
    }
}
