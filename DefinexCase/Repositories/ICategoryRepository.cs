﻿using DefinexCase.Repositories.Base;

namespace DefinexCase.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        //Task<Category> GetCategoryWithProductsAsync(int categoryId);
    }
}
