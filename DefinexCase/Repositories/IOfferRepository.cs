﻿using DefinexCase.Models;
using DefinexCase.Repositories.Base;
using System.Threading.Tasks;

namespace DefinexCase.Repositories
{
    public interface IOfferRepository : IRepository<Cart>
    {
        Task<OfferModel> FindBestOfferAsync(Cart cart);
    }
}
