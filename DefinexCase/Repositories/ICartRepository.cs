﻿using DefinexCase.Repositories.Base;
using System.Threading.Tasks;

namespace DefinexCase.Repositories
{
    public interface ICartRepository : IRepository<Cart>
    {
        Task<Cart> GetByUserNameAsync(string userName);
    }
}
