﻿using DefinexCase.Repositories.Base;
using System.Threading.Tasks;

namespace DefinexCase.Repositories
{
    public interface ICompareRepository : IRepository<Compare>
    {
        Task<Compare> GetByUserNameAsync(string userName);
    }
}
