﻿using System.Threading.Tasks;
using DefinexCase.Repositories.Base;

namespace DefinexCase.Repositories
{
    public interface IWishlistRepository : IRepository<Wishlist>
    {
        Task<Wishlist> GetByUserNameAsync(string userName);
    }
}
