﻿using DefinexCase.Models.Base;
using System.Collections.Generic;

namespace DefinexCase.Models
{
    public class OfferModel : BaseModel
    {
        public string OfferName { get; set; }
        public decimal? BestPrice { get; set; } 
    }
}
