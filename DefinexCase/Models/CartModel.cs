﻿using DefinexCase.Models.Base;
using System.Collections.Generic;

namespace DefinexCase.Models
{
    public class CartModel : BaseModel
    {
        public string UserName { get; set; }
        public List<CartItemModel> Items { get; set; } = new List<CartItemModel>();
    }
}
