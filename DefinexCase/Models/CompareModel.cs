﻿using DefinexCase.Models.Base;
using System.Collections.Generic;

namespace DefinexCase.Models
{
    public class CompareModel : BaseModel
    {
        public string UserName { get; set; }
        public List<ProductModel> Items { get; set; } = new List<ProductModel>();
    }
}
