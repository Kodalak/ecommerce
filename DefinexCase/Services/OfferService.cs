﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefinexCase.Interfaces;
using DefinexCase.Mapper;
using DefinexCase.Models;
using DefinexCase.Repositories;
using DefinexCase.Specifications;

namespace DefinexCase.Services
{
    public class OfferService : IOfferService
    {


        OfferModel model = new OfferModel();

        public OfferModel FindBestOffer(CartModel cart)
        {
            //Total
            decimal? Total = TotalCost(cart);

            //Offers

            bool CheckxThreeBuy_b = CheckxThreeBuy(cart);
            bool CheckBoneGone_b = CheckBoneGone(cart); // Need to be better algoritm
            bool CheckBigBag_b = CheckBigBag(cart);

            List<KeyValuePair<string,decimal?>> OfferList = new List<KeyValuePair<string,decimal?>>();

            if (CheckxThreeBuy_b)
            {
                decimal? ItemsTotal = cart.Items.Find(x => x.Quantity == 3)?.TotalPrice;                

                OfferList.Add(new KeyValuePair<string, decimal?>("xThreeBuy",Total - (ItemsTotal * 15) / 100));

            }
            if (CheckBoneGone_b)
            {
                // Offer product price
                decimal? BoneGone_Pp = 0;
                foreach (var item in cart.Items)
                {
                    if (item.Product.Description == "BoneGone")
                    {
                        BoneGone_Pp = item.Product.UnitPrice;
                        break;
                    }
                }


                OfferList.Add(new KeyValuePair<string, decimal?>("BoneGone", Total-BoneGone_Pp));
            }
            if (CheckBigBag_b)
            {
                var ChepestValue = cart.Items.Min(x => x.UnitPrice);


                OfferList.Add(new KeyValuePair<string, decimal?>("BigBag", Total - (ChepestValue * 20) / 100));
            }

            //offer rating
            if (OfferList.Count > 0)
            {
                OfferList = OfferList.OrderBy(x => x.Value).ToList();


                model.OfferName = OfferList[0].Key;
                model.BestPrice = OfferList[0].Value;

            }
            else
            {
                model.OfferName = "No Offer";
                model.BestPrice = Total;

            }


            return model;
        }

        public bool CheckxThreeBuy(CartModel cart)
        {
            if (cart.Items.Any(x => x.Quantity == 3))
            {
                return true;
            }

            return false;
        }

        public bool CheckBoneGone(CartModel cart)
        {
            List<int> list = new List<int>(); 
            foreach (var item in cart.Items)
            {
                if (item.Product.Description == "BoneGone" && item.Quantity >= 2)
                {
                    return true;
                }                
            }
            if (list.Count <= 1) return false;
            return list.Count != list.Distinct().Count();

        }

        public bool CheckBigBag(CartModel cart)
        {
            if (TotalCost(cart) > 1000) return true;
            else return false;      
        }


        public decimal? TotalCost(CartModel cart)
        {
            decimal? Total = 0;
            foreach (var item in cart.Items)
            {
                Total += item.TotalPrice;
            }

            return Total;
        }

    }
}
