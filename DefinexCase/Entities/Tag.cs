﻿using DefinexCase.Base;

namespace DefinexCase
{
    public class Tag : Entity
    {
        public string Name { get; set; }
    }
}
