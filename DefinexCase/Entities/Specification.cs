﻿using DefinexCase.Base;

namespace DefinexCase
{
    public class Specification : Entity
    {        
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
