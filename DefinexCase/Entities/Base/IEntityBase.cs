﻿namespace DefinexCase.Base
{
    public interface IEntityBase<TId>
    {
        TId Id { get; }
    }
}
