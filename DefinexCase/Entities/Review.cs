﻿using DefinexCase.Base;

namespace DefinexCase
{
    public class Review : Entity
    {
        public string Name { get; set; }
        public string EMail { get; set; }
        public string Comment { get; set; }        
        public double Star { get; set; }
    }
}
