﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefinexCase.Interfaces;
using DefinexCase.Models;
using DefinexCase.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DefinexCase.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IIndexPageService _indexPageService;
        private readonly ICartComponentService _cartComponentService;
        private readonly ICartService _cartAppService;

        public IndexModel(IIndexPageService indexPageService, ICartComponentService cartComponentService, ICartService cartService)
        {
            _indexPageService = indexPageService ?? throw new ArgumentNullException(nameof(indexPageService));
            _cartComponentService = cartComponentService ?? throw new ArgumentNullException(nameof(cartComponentService));
            _cartAppService = cartService ?? throw new ArgumentNullException(nameof(cartService));
        }

        [BindProperty]
        public IEnumerable<ProductViewModel> ProductList { get; set; } = new List<ProductViewModel>();
        public CategoryViewModel CategoryModel { get; set; } = new CategoryViewModel();

        public async Task OnGetAsync()
        {
            ProductList = await _indexPageService.GetProducts();
        }

        public async Task<IActionResult> OnPostRemoveToCartAsync(int cartId, int cartItemId)
        {
            await _cartComponentService.RemoveItem(cartId, cartItemId);
            return RedirectToPage();
        }

        public async Task OnGetAddToCart(int productId)
        {
            await _cartAppService.AddItem(User.Identity.Name, productId);
        }

        // For now, there is no very good method for counting. 
        public async Task<JsonResult> OnGetCartCounter()
        {
            if (User.Identity.IsAuthenticated)
            {
                var Count = await _cartAppService.GetCartCountByUserName(User.Identity.Name);
                return new JsonResult(Count);
            }
            else
            {
                return new JsonResult(0);
            }
        }

    }
}
