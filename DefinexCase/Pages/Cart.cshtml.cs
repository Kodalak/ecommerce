using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DefinexCase.Interfaces;
using DefinexCase.Models;
using DefinexCase.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DefinexCase.Pages
{
    [Authorize]
    public class CartModel : PageModel
    {

        private readonly ICartComponentService _cartComponentService;
        private readonly ICartService _cartAppService;
        private readonly IOfferService _offerService;

        public CartModel(ICartComponentService cartComponentService, ICartService cartService, IOfferService offerService)
        {
            _cartComponentService = cartComponentService ?? throw new ArgumentNullException(nameof(cartComponentService));
            _cartAppService = cartService ?? throw new ArgumentNullException(nameof(cartService));
            _offerService = offerService ?? throw new ArgumentNullException(nameof(cartService));
        }

        [BindProperty]
        public Models.CartModel CartList { get; set; } = new Models.CartModel();

        [BindProperty]
        public OfferModel Offer { get; set; } = new OfferModel();



        public async Task OnGetAsync()
        {
            //if (!User.Identity.IsAuthenticated)
            //{
            //    return Redirect("/Identity/Account/Login");
            //}

            CartList = await _cartAppService.GetCartByUserName(User.Identity.Name);
            // Find Offer
            Offer = _offerService.FindBestOffer(CartList);
            
        }


        // Cant get 2 parameters -- Need to be better
        public async Task OnGetRemoveFromCart(int cartItemId)
        {

            CartList = await _cartAppService.GetCartByUserName(User.Identity.Name);

            await _cartAppService.RemoveItem(CartList.Id, cartItemId);
        }


        public async Task<JsonResult> OnGetCartCounter()
        {
            var Count = await _cartAppService.GetCartCountByUserName(User.Identity.Name);
            return new JsonResult(Count);
        }



    }
}
