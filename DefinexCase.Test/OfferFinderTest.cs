using DefinexCase.Models;
using DefinexCase.Services;
using System;
using System.Collections.Generic;
using Xunit;

namespace DefinexCase.Test
{
    public class OfferFinderTest
    {

        [Fact]
        public void FindOffer_xThreeBuy_Success()
        {
            // Arrange
            //cardmodel
            var cardModel = new CartModel()
            {
                Id = 1,
                Items = new List<CartItemModel>() {
                new CartItemModel()
                {
                    Id = 1,
                    Color = "black",
                    ProductId = 1,
                    Product = new ProductModel()
                    {
                        Description = "",
                    },
                    Quantity = 3,
                    UnitPrice = 100,
                    TotalPrice = 300
                }
                },
                UserName = "test"
            };
            var offerService = new OfferService();

            // Act
            var offerModel = offerService.FindBestOffer(cardModel);

            // Assert
            Assert.NotNull(offerModel);
            Assert.Equal(255, offerModel.BestPrice);
            Assert.Equal("xThreeBuy", offerModel.OfferName);
        }

        [Fact]
        public void FindOffer_BoneGone_Success()
        {
            // Arrange
            //cardmodel
            var cardModel = new CartModel()
            {
                Id = 1,
                Items = new List<CartItemModel>() {
                new CartItemModel()
                {
                    Id = 1,
                    Color = "black",
                    ProductId = 1,
                    Product = new ProductModel()
                    {
                        UnitPrice = 100,
                        Description = "BoneGone",
                    },
                    Quantity = 3,
                    UnitPrice = 100,
                    TotalPrice = 300
                }
                },
                UserName = "test"
            };
            var offerService = new OfferService();

            // Act
            var offerModel = offerService.FindBestOffer(cardModel);

            // Assert
            Assert.NotNull(offerModel);
            Assert.Equal(200, offerModel.BestPrice);
            Assert.Equal("BoneGone", offerModel.OfferName);
        }

        [Fact]
        public void FindOffer_BigBag_Success()
        {
            // Arrange
            //cardmodel
            var cardModel = new CartModel()
            {
                Id = 1,
                Items = new List<CartItemModel>() {
                new CartItemModel()
                {
                    Id = 1,
                    Color = "black",
                    ProductId = 1,
                    Product = new ProductModel()
                    {
                        UnitPrice = 100,
                        Description = "",
                    },
                    Quantity = 2,
                    UnitPrice = 100,
                    TotalPrice = 200
                },
                                new CartItemModel()
                {
                    Id = 2,
                    Color = "black",
                    ProductId = 2,
                    Product = new ProductModel()
                    {
                        UnitPrice = 200,
                        Description = "",
                    },
                    Quantity = 2,
                    UnitPrice = 200,
                    TotalPrice = 400
                },
                                                new CartItemModel()
                {
                    Id = 3,
                    Color = "black",
                    ProductId = 3,
                    Product = new ProductModel()
                    {
                        UnitPrice = 500,
                        Description = "",
                    },
                    Quantity = 1,
                    UnitPrice = 500,
                    TotalPrice = 500
                }
                },
                UserName = "test"
            };
            var offerService = new OfferService();

            // Act
            var offerModel = offerService.FindBestOffer(cardModel);

            // Assert
            Assert.NotNull(offerModel);
            Assert.Equal(1080, offerModel.BestPrice);
            Assert.Equal("BigBag", offerModel.OfferName);
        }

    }
}
